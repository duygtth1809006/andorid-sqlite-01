package com.example.exercise09;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new DBHelper(this);

        insertUser();
        getAllUser();
        updateUser();
        deleteUser();
    }

    private void deleteUser() {
        String msg = dbHelper.deleteUser(5);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private void updateUser() {
        User user = new User();
        user.setId(3);
        user.setName("Nguyen Van B");
        user.setAge(30);
        String msg = dbHelper.updateUser(user);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private void getAllUser() {
        List<User> users = dbHelper.getAllUser();
        for(User user : users) {
            Log.d("USER", "Name: " + user.getName() + " Gender: " + user.getGender() + " Age: " + user.getAge());
        }
    }

    private void insertUser() {
        User user = new User("Nguyen Van A", "Male", 20);
        String message = dbHelper.insertUser(user);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        dbHelper.close();
    }
}