package com.example.exercise09;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class DBHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "User.db";
    public static final String TABLE_NAME = "TBL_USER";
    public static final int DB_VERSION = 1;

    public DBHelper(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + TABLE_NAME + " ( " +
                TBL_USER.id + "INTEGER PRIMARY KEY, " +
                TBL_USER.name + " TEXT, " +
                TBL_USER.gender + " TEXT," +
                TBL_USER.gender + " INTEGER )";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public String insertUser(User user) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TBL_USER.name, user.getName());
        values.put(TBL_USER.gender, user.getGender());
        values.put(TBL_USER.age, user.getAge());
        long isSuccess = database.insert(TABLE_NAME, null, values);
        if(isSuccess > 0) {
            return "Success";
        } else {
            return "Fail";
        }
    }

    public List<User> getAllUser() {
        SQLiteDatabase database = this.getWritableDatabase();
        String sql = "select * from " + TABLE_NAME;
        List<User> users = new ArrayList<>();
        Cursor cursor = database.rawQuery(sql, null);
        if(cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                User user = new User();
                user.setId(cursor.getInt(cursor.getColumnIndex(TBL_USER.id)));
                user.setName(cursor.getString(cursor.getColumnIndex(TBL_USER.name)));
                user.setAge(cursor.getInt(cursor.getColumnIndex(TBL_USER.age)));
                user.setGender(cursor.getString(cursor.getColumnIndex(TBL_USER.gender)));
                users.add(user);
            } while (cursor.moveToNext());
        }
        return users;
    }

    public String updateUser(User user) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TBL_USER.name, user.getName());
        values.put(TBL_USER.age, user.getAge());
        long isSuccess = database.update(TABLE_NAME, values, "id="+user.getId(),null);
        if(isSuccess > 0) {
            return "Update Success";
        } else {
            return "Update Fail";
        }
    }

    public String deleteUser(int user_id) {
        SQLiteDatabase database = this.getWritableDatabase();
        long isSuccess = database.delete(TABLE_NAME, "id="+user_id,null);
        if(isSuccess > 0) {
            return "Update Success";
        } else {
            return "Update Fail";
        }
    }
}
